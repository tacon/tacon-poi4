# tacon-poi5

Set of utilities to simplify report creation with [Apache POI 5](https://poi.apache.org/)

## Installation

Add Tacon repository:
```xml
<repositories>
	<repository>
		<id>gitlab-maven-tacon</id>
		<url>https://gitlab.com/api/v4/groups/tacon/-/packages/maven</url>
	</repository>
</repositories>
```

Use Maven depencency:
```xml
<dependency>
	<groupId>dev.tacon.poi5</groupId>
	<artifactId>tacon-poi5</artifactId>
	<version>0.1.0</version>
<dependency>
```

[Apache POI 5](https://poi.apache.org/) libraries are not provided, include them in the dependencies:
```xml
<dependency>
	<groupId>org.apache.poi</groupId>
	<artifactId>poi</artifactId>
	<version>5.2.3</version>
	<scope>provided</scope>
</dependency>
<dependency>
	<groupId>org.apache.poi</groupId>
	<artifactId>poi-ooxml</artifactId>
	<version>5.2.3</version>
	<scope>provided</scope>
</dependency>
```

## Usage

### Simple single sheet

Exporter class:
```java
public static class SimplePeopleExcelExporter extends FunctionalExcelExporter<Person> {
	
	public SimplePeopleExcelExporter(final List<Person> dataList, final String name) {
		super(dataList, name);
	}
	
	@Override
	protected List<CellFiller<Person, ?>> getCellFillers() {
		final List<CellFiller<Person, ?>> cellFillers = List.of(
				CellFiller.forString(p -> p.lastName()),
				CellFiller.forString(p -> p.name()),
				CellFiller.forLocalDate(p -> p.birthDate(), "dd/MM/yyyy"),
				CellFiller.<Person> forInteger(x -> Integer.valueOf(Period.between(x.birthDate(), LocalDate.now()).getYears())),
				CellFiller.<Person> forDecimal(x -> x.amount()));
		return cellFillers;
	}
	
	@Override
	protected String[] getHeader() {
		return new String[] { "Last name", "Name", "Birth date", "Age", "Amount" };
	}
}
```

Use exporter class:
```java
final List<Person> people = List.of(
		new Person("Alice", "White", LocalDate.of(2000, 1, 1), new BigDecimal("123.456")),
		new Person("Bob", "White", LocalDate.of(2001, 2, 2), new BigDecimal("100")),
		new Person("Charlie", "Green", LocalDate.of(2002, 3, 1), new BigDecimal("200.01")));

final SimplePeopleExcelExporter excelExporter = new SimplePeopleExcelExporter(people, "PeopleData");

try (OutputStream os = new FileOutputStream(new File("simple.xlsx"))) {
	excelExporter.export(os, ExcelFormat.XLSX);
}
```

### Single sheet with groups

Exporter class:
```java
public static class GroupPeopleExcelExporter extends FunctionalExcelExporter<Person> {

	public GroupPeopleExcelExporter(final List<Person> dataList, final String name) {
		super(dataList, name);
	}

	@Override
	protected List<CellFiller<Person, ?>> getCellFillers() {
		final List<CellFiller<Person, ?>> cellFillers = List.of(
				CellFiller.forString(p -> p.lastName()),
				CellFiller.forString(p -> p.name()),
				CellFiller.forLocalDate(p -> p.birthDate(), "dd/MM/yyyy"),
				CellFiller.<Person> forInteger(x -> Integer.valueOf(Period.between(x.birthDate(), LocalDate.now()).getYears())),
				CellFiller.<Person> forDecimal(x -> x.amount()));
		return cellFillers;
	}

	@Override
	protected List<Group<Person, ?>> getGroups() {
		final Group<Person, BigDecimal> lastNameGroup = new Group<>(
				p -> p.lastName(),
				new Accumulator<Person, BigDecimal>() {

					@Override
					protected BigDecimal getInitialValue() {
						return BigDecimal.ZERO;
					}

					@Override
					protected BigDecimal accumulate1(final BigDecimal currentValue, final Person dto) {
						return currentValue.add(dto.amount());
					}

				},

				(w, d) -> {
					final Row row = w.createRow(w.getLastRowNum() + 1);

					final Cell cell = row.createCell(0);
					cell.setCellValue("People with last name: " + d.lastName());
					row.getSheet().addMergedRegion(new CellRangeAddress(row.getRowNum(), row.getRowNum(), 0, 4));

				},
				(w, d, v) -> {
					final Row row = w.createRow(w.getLastRowNum() + 1);
					final Cell cell = row.createCell(0);
					cell.setCellValue("Total ");
					row.getSheet().addMergedRegion(new CellRangeAddress(row.getRowNum(), row.getRowNum(), 0, 3));

					final Cell cell1 = row.createCell(4);
					CellFiller.forDecimal().compileCell(cell1, v);
				});
		final Group<Person, BigDecimal> granTotal = new Group<>(
				p -> "",
				new Accumulator<Person, BigDecimal>() {

					@Override
					protected BigDecimal getInitialValue() {
						return BigDecimal.ZERO;
					}

					@Override
					protected BigDecimal accumulate1(final BigDecimal currentValue, final Person dto) {
						return currentValue.add(dto.amount());
					}

				},
				(w, d, v) -> {
					final Row row = w.createRow(w.getLastRowNum() + 2);
					final Cell cell = row.createCell(0);
					cell.setCellValue("Total ");
					row.getSheet().addMergedRegion(new CellRangeAddress(row.getRowNum(), row.getRowNum(), 0, 3));

					final Cell cell1 = row.createCell(4);
					CellFiller.forDecimal().compileCell(cell1, v);

				});
		return List.of(granTotal, lastNameGroup);
	}

	@Override
	protected String[] getHeader() {
		return new String[] { "Last name", "Name", "Birth date", "Age", "Amount" };
	}
}
```

Use exporter class:
```java
final List<Person> people = List.of(
		new Person("Alice", "White", LocalDate.of(2000, 1, 1), new BigDecimal("123.456")),
		new Person("Bob", "White", LocalDate.of(2001, 2, 2), new BigDecimal("100")),
		new Person("Charlie", "Green", LocalDate.of(2002, 3, 1), new BigDecimal("200.01")));

final GroupPeopleExcelExporter excelExporter = new GroupPeopleExcelExporter(people, "PeopleData");

try (OutputStream os = new FileOutputStream(new File("fun.xlsx"))) {
	excelExporter.export(os, ExcelFormat.XLSX);
}
```
