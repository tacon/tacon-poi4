package dev.tacon.poi5;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * Abstract class for exporting data to Excel format.
 * Defines basic structure and utility methods for exporting data.
 */
public abstract class ExcelExporter {

	/**
	 * Enum representing the Excel file formats.
	 */
	public enum ExcelFormat {

		/**
		 * Represents the older Excel file format (.xls).
		 */
		XLS(".xls") {

			@Override
			protected Workbook newWorkBook() {
				return new HSSFWorkbook();
			}
		},
		/**
		 * Represents the newer Excel file format (.xlsx).
		 */
		XLSX(".xlsx") { // NO_UCD (unused code)

			@Override
			protected Workbook newWorkBook() {
				return new XSSFWorkbook();
			}
		};

		private final String dotExtension;

		ExcelFormat(final String dotExtension) {
			this.dotExtension = dotExtension;
		}

		/**
		 * Abstract method to create a new Workbook instance.
		 *
		 * @return A new Workbook object.
		 */
		protected abstract Workbook newWorkBook();

		/**
		 * Get the extension string for the Excel format.
		 *
		 * @return The extension string, including the dot.
		 */
		public String getDotExtension() {
			return this.dotExtension;
		}
	}

	/**
	 * Abstract method for filling a Workbook object.
	 * Subclasses must implement this method.
	 *
	 * @param wbook The Workbook to be filled.
	 */
	protected abstract void fillWorkbook(Workbook wbook);

	/**
	 * Build header cells in an Excel row.
	 *
	 * @param workbook The Workbook containing the row.
	 * @param row The Row to insert the header cells into.
	 * @param captions The header captions.
	 */
	protected void buildHeaderCells(final Workbook workbook, final Row row, final String... captions) {
		// creo lo stile delle celle della riga di testata
		final CellStyle headerCellStyle = workbook.createCellStyle();
		final Font headerCellFont = workbook.createFont();
		headerCellFont.setBold(true);
		headerCellStyle.setFont(headerCellFont);

		for (int i = 0; i < captions.length; i++) {
			final Cell cell = row.createCell(i);
			cell.setCellStyle(headerCellStyle);
			cell.setCellValue(captions[i]);
		}
	}

	/**
	 * Build data cells in an Excel row.
	 *
	 * @param row The row to be filled.
	 * @param data The data object.
	 * @param fillers The list of CellFiller instances to use for filling cells.
	 */
	protected <D> void buildDataCells(final Row row, final D data, final List<CellFiller<D, ?>> fillers) {
		for (int i = 0; i < fillers.size(); i++) {
			fillers.get(i).compileCell(row.createCell(i), data);
		}
	}

	/**
	 * Export data to an Excel file.
	 *
	 * @param os OutputStream where the Excel file will be written.
	 * @param format The Excel format.
	 * @throws IOException If an IO error occurs.
	 */
	public void export(final OutputStream os, final ExcelFormat format) throws IOException {
		try (final Workbook workbook = format.newWorkBook()) {
			this.fillWorkbook(workbook);
			workbook.write(os);
		}
	}

	/**
	 * Export data to an Excel InputStream.
	 *
	 * @param format The Excel format.
	 * @return An InputStream containing the Excel data.
	 * @throws IOException If an IO error occurs.
	 */
	public InputStream export(final ExcelFormat format) throws IOException {
		try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
			this.export(baos, format);
			return new ByteArrayInputStream(baos.toByteArray());
		}
	}

	/**
	 * Append a new row to an Excel sheet.
	 *
	 * @param sheet The Sheet to append the new row to.
	 * @return The newly appended row.
	 */
	protected static Row appendRow(final Sheet sheet) {
		final int lastRowNum = sheet.getLastRowNum();
		return sheet.createRow(lastRowNum == 0 ? sheet.getPhysicalNumberOfRows() : lastRowNum + 1);
	}
}