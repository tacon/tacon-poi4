package dev.tacon.poi5;

import org.apache.poi.ss.usermodel.Sheet;

/**
 * A functional interface for building group footers in a {@link org.apache.poi.ss.usermodel.Sheet Sheet}
 * based on data transfer objects (DTOs) of type {@code D} and accumulated values of type {@code V}.
 *
 * <p>This interface is designed to be used as a lambda or method reference in functional-style programming.
 *
 * @param <D> The type of data transfer object used for building the footer.
 * @param <V> The type of the accumulated value used for building the footer.
 *
 *            <p>Example usage:
 *
 *            <pre>
 * {@code
 *   GroupFooterBuilder<String, Integer> footerBuilder = (sheet, dto, accValue) -> {
 *       // custom footer building logic here
 * };
 * }
 * </pre>
 */
@FunctionalInterface
public interface GroupFooterBuilder<D, V> {

	/**
	 * A no-operation (noop) implementation of the GroupFooterBuilder interface,
	 * that performs no actions.
	 */
	GroupFooterBuilder<?, ?> DO_NOTHING = (w, d, a) -> {};

	/**
	 * Returns a no-operation implementation of the {@code GroupFooterBuilder} interface.
	 *
	 * @param <D> The type of data transfer object.
	 * @param <A> The type of the accumulated value.
	 * @return A no-operation implementation of the {@code GroupFooterBuilder}.
	 */
	@SuppressWarnings("unchecked")
	static <D, A> GroupFooterBuilder<D, A> getDoNothing() {
		return (GroupFooterBuilder<D, A>) DO_NOTHING;
	}

	/**
	 * Builds the edge (footer) in the provided sheet based on the provided DTO object
	 * and an accumulated value.
	 * 
	 * <p>Implementations should define how to use the sheet, the DTO, and the accumulated value
	 * to construct a footer.
	 *
	 * @param workbook The sheet in which the footer will be built.
	 * @param dto The data transfer object used to build the footer.
	 * @param accumulatorValue The accumulated value used to build the footer.
	 */
	void buildEdge(Sheet workbook, D dto, V accumulatorValue);
}