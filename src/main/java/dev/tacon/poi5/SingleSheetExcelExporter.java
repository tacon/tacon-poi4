package dev.tacon.poi5;

import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

/**
 * Concrete class extending {@link ExcelExporter} for exporting data to a single Excel sheet.
 *
 * @param <T> The type of objects that will be used as the data source for the Excel sheet.
 */
public abstract class SingleSheetExcelExporter<T> extends ExcelExporter {

	/**
	 * The list of data items to be exported to Excel.
	 */
	private final List<T> dataSource;
	/**
	 * The name of the Excel sheet.
	 */
	private final String name;

	/**
	 * Constructor for initializing data source and sheet name.
	 *
	 * @param dataSource A list of data items to be exported.
	 * @param name The name of the Excel sheet.
	 */
	public SingleSheetExcelExporter(final List<T> dataSource, final String name) {
		this.dataSource = dataSource;
		this.name = name;
	}

	/**
	 * Abstract method for defining the headers of the Excel sheet.
	 * Must be implemented by subclasses.
	 *
	 * @return An array of header strings.
	 */
	protected abstract String[] getHeader();

	/**
	 * Abstract method for filling the Excel sheet with data.
	 * Must be implemented by subclasses.
	 *
	 * @param data The list of data items.
	 * @param workbook The workbook where the sheet belongs.
	 * @param sheet The sheet to be filled.
	 */
	protected abstract void fillSheet(final List<T> data, final Workbook workbook, final Sheet sheet);

	/**
	 * Implementation of the abstract method from {@link ExcelExporter}.
	 * Fills the Excel workbook with data.
	 *
	 * @param workbook The workbook to be filled.
	 */
	@Override
	protected void fillWorkbook(final Workbook workbook) {
		final Sheet sheet = workbook.createSheet(this.name);

		final CellStyle headerCellStyle = workbook.createCellStyle();
		final Font headerCellFont = workbook.createFont();
		headerCellFont.setBold(true);
		headerCellStyle.setFont(headerCellFont);
		this.fillHeader(sheet, headerCellStyle);

		if (!this.dataSource.isEmpty()) {
			this.fillSheet(this.dataSource, workbook, sheet);
		}
	}

	/**
	 * Fill the header row of the Excel sheet.
	 *
	 * @param sheet The sheet where the header row will be created.
	 * @param headerCellStyle The cell style for header cells.
	 */
	protected void fillHeader(final Sheet sheet, final CellStyle headerCellStyle) {
		final Row headerRow = appendRow(sheet);
		final String[] headerCaptions = this.getHeader();
		for (int i = 0; i < headerCaptions.length; i++) {
			final Cell cell = headerRow.createCell(i);
			cell.setCellStyle(headerCellStyle);
			cell.setCellValue(headerCaptions[i]);
		}
	}
}
