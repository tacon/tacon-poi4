package dev.tacon.poi5;

import java.util.Collections;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

/**
 * This abstract class represents an Excel exporter that can be used to export generic data to an Excel sheet
 * with a functional interface for populating cells.
 *
 * @param <T> The type of data to be exported.
 */
public abstract class FunctionalExcelExporter<T> extends SingleSheetExcelExporter<T> {

	/**
	 * Constructor for the FunctionalExcelExporter class.
	 *
	 * @param dataList The list of data to be exported.
	 * @param name The name of the Excel sheet.
	 */
	public FunctionalExcelExporter(final List<T> dataList, final String name) {
		super(dataList, name);
	}

	/**
	 * Get a list of CellFiller objects that define how to fill the cells in the Excel sheet.
	 *
	 * @return A list of CellFiller objects.
	 */
	protected abstract List<CellFiller<T, ?>> getCellFillers();

	/**
	 * Get a list of Group objects that define how to group and aggregate data in the Excel sheet.
	 *
	 * @return A list of Group objects.
	 */
	protected List<Group<T, ?>> getGroups() {
		return Collections.emptyList();
	}

	/**
	 * Compile a row in the Excel sheet using the specified cell fillers and data bean.
	 *
	 * @param row The row in which to compile data.
	 * @param cellFillers The list of cell fillers to use for populating cells.
	 * @param bean The data bean from which to populate cells.
	 * @return The number of columns used in the row.
	 */
	protected int compileRow(final Row row, final List<CellFiller<T, ?>> cellFillers, final T bean) {
		int col = 0;
		for (final CellFiller<T, ?> cellFiller : cellFillers) {
			final Cell cell = row.createCell(col);
			cellFiller.compileCell(cell, bean);
			col++;
		}
		return col;
	}

	/**
	 * Fill the Excel sheet with data using the specified data source, workbook, and sheet.
	 *
	 * @param dataSource The data source to export to the Excel sheet.
	 * @param workbook The Excel workbook.
	 * @param sheet The Excel sheet to populate.
	 */
	@Override
	protected void fillSheet(final List<T> dataSource, final Workbook workbook, final Sheet sheet) {
		int usedColumns = 0;
		final List<CellFiller<T, ?>> cellFillers = this.getCellFillers();

		@SuppressWarnings("unchecked")
		final Group<T, ?>[] groups = this.getGroups().toArray(new Group[0]);

		final boolean[] groupsOpen = new boolean[groups.length];

		for (final T data : dataSource) {
			int lastGroupIndex = groups.length;
			for (int i = groups.length - 1; i >= 0; i--) {
				if (groups[i].changed(data)) {
					lastGroupIndex = i;
				}
			}
			for (int i = groups.length - 1; i >= lastGroupIndex; i--) {
				final Group<T, ?> group = groups[i];
				if (groupsOpen[i]) {
					group.buildFooter(sheet);
					groupsOpen[i] = false;
				}
			}

			for (final Group<T, ?> group : groups) {
				group.accumulate(data);
			}

			for (int i = lastGroupIndex; i < groups.length; i++) {
				groupsOpen[i] = true;
				final Group<T, ?> group = groups[i];
				group.buildHeader(sheet, data);
			}

			final Row row = sheet.createRow(sheet.getLastRowNum() + 1);
			final int usedCols = this.compileRow(row, cellFillers, data);
			usedColumns = Math.max(usedColumns, usedCols);

		}
		for (int i = groups.length - 1; i >= 0; i--) {
			if (groupsOpen[i]) {
				groups[i].buildFooter(sheet);
				groupsOpen[i] = false;
			}
		}

		for (int i = 0; i < usedColumns; i++) {
			sheet.autoSizeColumn(i);
		}
	}
}
