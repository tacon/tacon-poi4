package dev.tacon.poi5;

import java.util.Objects;
import java.util.function.Function;

import org.apache.poi.ss.usermodel.Sheet;

/**
 * Represents a Group that can be used for building headers and footers in a {@link org.apache.poi.ss.usermodel.Sheet Sheet},
 * based on data transfer objects (DTOs) of type {@code D} and accumulated values of type {@code A}.
 *
 * @param <D> The type of the data transfer object used for building headers and footers.
 * @param <A> The type of the accumulated value used for building footers.
 *
 *            <p>Example Usage:
 *
 *            <pre>
 * {@code
 *   Function<String, ?> evalFunc = ...;  // Evaluation function
 *   Accumulator<String, Integer> acc = ...;  // Your accumulator
 *   GroupHeaderBuilder<String> headerBuilder = ...;  // Your header builder
 *   GroupFooterBuilder<String, Integer> footerBuilder = ...;  // Your footer builder
 *
 *   Group<String, Integer> group = new Group<>(evalFunc, acc, headerBuilder, footerBuilder);
 * }
 * </pre>
 */
public class Group<D, A> {

	private Object oldValue;
	private final Function<D, ?> evalFunction;

	private final GroupHeaderBuilder<D> headerBuilder;

	private final GroupFooterBuilder<D, A> footerBuilder;

	private final Accumulator<D, A> accumulator;

	/**
	 * Main constructor for creating a Group.
	 *
	 * @param evalFunction The function used to evaluate change in the group.
	 * @param accumulator An Accumulator for holding state across multiple DTOs.
	 * @param headerBuilder A builder for generating headers.
	 * @param footerBuilder A builder for generating footers.
	 */
	public Group(final Function<D, ?> evalFunction, final Accumulator<D, A> accumulator, final GroupHeaderBuilder<D> headerBuilder, final GroupFooterBuilder<D, A> footerBuilder) {
		this.evalFunction = evalFunction;
		this.accumulator = accumulator;
		this.headerBuilder = headerBuilder;
		this.footerBuilder = footerBuilder;
	}

	/**
	 * Creates a new Group with the specified evaluation function and header builder.
	 * The accumulator and footer builder are set to their respective "do nothing" implementations.
	 *
	 * @param evalFunction The function used to evaluate a change based on the data transfer object of type {@code D}.
	 * @param headerBuilder The builder for generating group headers.
	 */
	public Group(final Function<D, ?> evalFunction, final GroupHeaderBuilder<D> headerBuilder) {
		this(evalFunction, Accumulator.getDummyAccumulator(), headerBuilder, GroupFooterBuilder.getDoNothing());
	}

	/**
	 * Creates a new Group with the specified evaluation function, accumulator, and footer builder.
	 * The header builder is set to its "do nothing" implementation.
	 *
	 * @param evalFunction The function used to evaluate a change based on the data transfer object of type {@code D}.
	 * @param accumulator The accumulator for holding state across multiple DTOs.
	 * @param footerBuilder The builder for generating group footers.
	 */
	public Group(final Function<D, ?> evalFunction, final Accumulator<D, A> accumulator, final GroupFooterBuilder<D, A> footerBuilder) {
		this(evalFunction, accumulator, GroupHeaderBuilder.getDoNothing(), footerBuilder);
	}

	/**
	 * Creates a new Group with the specified evaluation function.
	 * The accumulator, header builder, and footer builder are set to their respective "do nothing" implementations.
	 *
	 * @param evalFunction The function used to evaluate a change based on the data transfer object of type {@code D}.
	 */
	public Group(final Function<D, ?> evalFunction) {
		this(evalFunction, Accumulator.getDummyAccumulator(), GroupHeaderBuilder.getDoNothing(), GroupFooterBuilder.getDoNothing());
	}

	/**
	 * Checks whether a change has occurred in the group.
	 *
	 * @param dto The current data transfer object.
	 * @return true if a change occurred, false otherwise.
	 */
	public boolean changed(final D dto) {

		final Object currentValue = this.evalFunction.apply(dto);
		final boolean b = Objects.equals(currentValue, this.oldValue);
		this.oldValue = currentValue;
		return !b;
	}

	/**
	 * Updates the accumulator with the current DTO.
	 *
	 * @param dto The current data transfer object.
	 */
	public void accumulate(final D dto) {
		this.accumulator.accumulate(dto);
	}

	/**
	 * Builds a header in the provided sheet based on the current DTO.
	 *
	 * @param sheet The sheet where the header will be built.
	 * @param dto The current data transfer object.
	 */
	public void buildHeader(final Sheet sheet, final D dto) {
		this.headerBuilder.buildEdge(sheet, dto);
	}

	/**
	 * Builds a footer in the provided sheet.
	 *
	 * @param sheet The sheet where the footer will be built.
	 */
	public void buildFooter(final Sheet sheet) {
		this.footerBuilder.buildEdge(sheet, this.accumulator.getLast(), this.accumulator.getValue());
	}
}