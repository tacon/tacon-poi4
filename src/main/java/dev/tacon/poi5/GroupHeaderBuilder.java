package dev.tacon.poi5;

import org.apache.poi.ss.usermodel.Sheet;

/**
 * A functional interface for building group headers in a {@link org.apache.poi.ss.usermodel.Sheet Sheet}
 * based on data transfer objects (DTOs) of type {@code D}.
 *
 * <p>This interface is designed to be used as a lambda or method reference in functional-style programming.
 *
 * @param <D> The type of data transfer object used for building the header.
 *
 *            <p>Example usage:
 *
 *            <pre>
 * {@code
 *   GroupHeaderBuilder<String> headerBuilder = (sheet, dto) -> {
 *       // custom header building logic
 * };
 * }
 * </pre>
 */
@FunctionalInterface
public interface GroupHeaderBuilder<D> {

	/**
	 * A no-operation (noop) implementation of the GroupHeaderBuilder interface,
	 * that performs no actions.
	 */
	GroupHeaderBuilder<?> DO_NOTHING = (w, d) -> {};

	/**
	 * Returns a no-operation implementation of the {@code GroupHeaderBuilder} interface.
	 *
	 * @param <T> The type of data transfer object.
	 * @return A no-operation implementation of the {@code GroupHeaderBuilder}.
	 */
	@SuppressWarnings("unchecked")
	static <T> GroupHeaderBuilder<T> getDoNothing() {
		return (GroupHeaderBuilder<T>) DO_NOTHING;
	}

	/**
	 * Builds the edge (header) in the provided sheet based on the provided DTO object.
	 *
	 * <p>Implementations should define how to use the sheet and the DTO to construct a header.
	 *
	 * @param workbook The sheet in which the header will be built.
	 * @param dto The data transfer object used to build the header.
	 */
	void buildEdge(Sheet workbook, D dto);
}
