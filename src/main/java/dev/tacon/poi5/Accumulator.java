package dev.tacon.poi5;

/**
 * Abstract class representing a generic Accumulator for accumulating values of type {@code A}
 * based on objects of type {@code D}.
 *
 * @param <D> The type of object to be used for accumulation.
 * @param <A> The type of accumulated value.
 *
 *            <p>Example Usage:
 *
 *            <pre>
 * {@code
 *   Accumulator<Integer, Integer> accumulator = new Accumulator<>() {
 *       protected Integer getInitialValue() {
 *           return 0;
 * }
 *
 * protected Integer accumulate1(Integer currentValue, Integer dto) {
 * return currentValue + dto;
 * }
 * };
 *
 * accumulator.accumulate(5);
 * accumulator.accumulate(10);
 * int value = accumulator.getValue(); // Should return 15
 * }
 * </pre>
 */
public abstract class Accumulator<D, A> {

	@SuppressWarnings("rawtypes")
	private static Accumulator DUMMY = new Accumulator() {

		@Override
		protected Object getInitialValue() {
			return null;
		}

		@Override
		protected Object accumulate1(final Object currentValue, final Object dto) {
			return currentValue;
		}
	};

	/**
	 * Returns a dummy accumulator that performs no operation.
	 *
	 * @param <D> The type of object to be used for accumulation.
	 * @param <A> The type of accumulated value.
	 * @return A dummy accumulator.
	 */
	@SuppressWarnings("unchecked")
	public static <D, A> Accumulator<D, A> getDummyAccumulator() {
		return DUMMY;
	}

	private A acc;
	private D last;

	/**
	 * Constructor initializes the accumulator with an initial value.
	 */
	public Accumulator() {
		this.acc = this.getInitialValue();
	}

	/**
	 * This method should return the initial value for the accumulator.
	 *
	 * @return the initial value for the accumulator.
	 */
	protected abstract A getInitialValue();

	/**
	 * Accumulates the values based on the provided DTO object.
	 *
	 * @param dto The data object used for accumulation.
	 */
	protected final void accumulate(final D dto) {
		this.last = dto;
		this.acc = this.accumulate1(this.acc, dto);
	}

	/**
	 * Defines the accumulation logic. Subclasses should implement this method to specify
	 * how to perform the accumulation.
	 *
	 * @param currentValue The current value of the accumulator.
	 * @param dto The data object used for accumulation.
	 * @return The new accumulated value.
	 */
	protected abstract A accumulate1(A currentValue, D dto);

	/**
	 * Gets the current value of the accumulator and resets it to the initial value.
	 *
	 * @return The current value of the accumulator.
	 */
	A getValue() {
		final A acc2 = this.acc;
		this.acc = this.getInitialValue();
		return acc2;
	}

	/**
	 * Gets the last data object used in the accumulation.
	 *
	 * @return The last data object used for accumulation.
	 */
	public D getLast() {
		return this.last;
	}
}
