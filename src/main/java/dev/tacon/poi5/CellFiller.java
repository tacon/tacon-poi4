package dev.tacon.poi5;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Objects;
import java.util.function.BiFunction;
import java.util.function.Function;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Workbook;

/**
 * A utility class for filling cell values in an Excel sheet.
 * This class provides multiple static methods for generating cell fillers for different data types.
 *
 * @param <D> the type of the data object
 * @param <K> the type of the cell value
 */
public class CellFiller<D, K> {

	/**
	 * Factory method for creating a DateCellFiller for handling Date data.
	 *
	 * @param valueProvider The function responsible for extracting Date values from D.
	 * @param pattern The date pattern to be used for formatting.
	 * @return A DateCellFiller instance.
	 */
	public static <D> CellFiller<D, Date> forDate(final Function<D, Date> valueProvider, final String pattern) {
		return new DateCellFiller<>(pattern, valueProvider);
	}

	/**
	 * Factory method for creating a DateCellFiller.
	 *
	 * @param pattern The date pattern to be used for formatting.
	 * @return A DateCellFiller instance.
	 */
	public static CellFiller<Date, Date> forDate(final String pattern) {
		return forDate(e -> e, pattern);
	}

	/**
	 * Factory method for creating a LocalDateCellFiller for handling LocalDate data.
	 *
	 * @param valueProvider The function responsible for extracting LocalDate values from D.
	 * @param pattern The date pattern to be used for formatting.
	 * @return A LocalDateCellFiller instance.
	 */
	public static <D> CellFiller<D, LocalDate> forLocalDate(final Function<D, LocalDate> valueProvider, final String pattern) {
		return new LocalDateCellFiller<>(pattern, valueProvider);
	}

	/**
	 * Factory method for creating a LocalDateCellFiller.
	 *
	 * @param pattern The date pattern to be used for formatting.
	 * @return A LocalDateCellFiller instance.
	 */
	public static CellFiller<LocalDate, LocalDate> forLocalDate(final String pattern) {
		return forLocalDate(e -> e, pattern);
	}

	/**
	 * Factory method for creating a LocalDateTimeCellFiller for handling LocalDateTime data.
	 *
	 * @param valueProvider The function responsible for extracting LocalDateTime values from D.
	 * @param pattern The date-time pattern to be used for formatting.
	 * @return A LocalDateTimeCellFiller instance.
	 */
	public static <D> CellFiller<D, LocalDateTime> forLocalDateTime(final Function<D, LocalDateTime> valueProvider, final String pattern) {
		return new LocalDateTimeCellFiller<>(pattern, valueProvider);
	}

	/**
	 * Factory method for creating a LocalDateTimeCellFiller.
	 *
	 * @param pattern The date-time pattern to be used for formatting.
	 * @return A LocalDateTimeCellFiller instance.
	 */
	public static CellFiller<LocalDateTime, LocalDateTime> forLocalDateTime(final String pattern) {
		return forLocalDateTime(e -> e, pattern);
	}

	/**
	 * Factory method for creating a BooleanCellFiller for handling Boolean data.
	 *
	 * @param valueProvider The function responsible for extracting Boolean values from D.
	 * @return A BooleanCellFiller instance.
	 */
	public static <D> CellFiller<D, Boolean> forBoolean(final Function<D, Boolean> valueProvider) {
		return new BooleanCellFiller<>(valueProvider);
	}

	/**
	 * Factory method for creating a NumericCellFiller for handling Integer data.
	 *
	 * @param valueProvider The function responsible for extracting Number values from D.
	 * @return A NumericCellFiller instance with an integer format.
	 */
	public static <D> CellFiller<D, Number> forInteger(final Function<D, Number> valueProvider) {
		return new NumericCellFiller<>("0", valueProvider);
	}

	/**
	 * Factory method for creating a NumericCellFiller for handling Integer data with default value provider.
	 *
	 * @return A NumericCellFiller instance with an integer format.
	 */
	public static CellFiller<Number, Number> forInteger() {
		return forInteger(e -> e);
	}

	/**
	 * Factory method for creating a NumericCellFiller for handling decimal data with a custom pattern.
	 *
	 * @param valueProvider The function responsible for extracting Number values from D.
	 * @param pattern The pattern to be used for formatting decimal numbers.
	 * @return A NumericCellFiller instance with a decimal format.
	 */

	public static <D> CellFiller<D, Number> forDecimal(final Function<D, Number> valueProvider, final String pattern) {
		return new NumericCellFiller<>(pattern, valueProvider);
	}

	/**
	 * Factory method for creating a NumericCellFiller for handling decimal data with 2 digits pattern.
	 *
	 * @param valueProvider The function responsible for extracting Number values from D.
	 * @return A NumericCellFiller instance with default decimal format.
	 */
	public static <D> CellFiller<D, Number> forDecimal(final Function<D, Number> valueProvider) {
		return new NumericCellFiller<>("#0.00", valueProvider);
	}

	/**
	 * Factory method for creating a NumericCellFiller for handling decimal data with default value provider and default pattern.
	 *
	 * @return A NumericCellFiller instance with default decimal format.
	 */
	public static CellFiller<Number, Number> forDecimal() {
		return forDecimal(e -> e);
	}

	/**
	 * Factory method for creating a CellFiller for handling String data.
	 *
	 * @param valueProvider The function responsible for extracting String values from D.
	 * @return A CellFiller instance for handling String data.
	 */
	public static <D> CellFiller<D, String> forString(final Function<D, String> valueProvider) {
		return new CellFiller<>("", valueProvider);
	}

	/**
	 * Factory method for creating a CellFiller for handling String data.
	 *
	 * @return A CellFiller instance for handling String data.
	 */
	public static CellFiller<String, String> forString() {
		return forString(e -> e);
	}

	/**
	 * Factory method for creating a FormulaCellFiller for handling formula data with default pattern.
	 *
	 * @param valueProvider The function responsible for extracting and generating formulae based on Cell and D.
	 * @return A FormulaCellFiller instance.
	 */
	public static <D> CellFiller<D, String> forFormula(final BiFunction<Cell, D, String> valueProvider) {
		return forFormula("", valueProvider);
	}

	/**
	 * Factory method for creating a FormulaCellFiller for handling formula data.
	 *
	 * @param valueProvider The function responsible for extracting and generating formulae based on Cell and D.
	 * @return A FormulaCellFiller instance.
	 */
	public static <D> CellFiller<D, String> forFormula(final String pattern, final BiFunction<Cell, D, String> valueProvider) {
		return new FormulaCellFiller<>(pattern, valueProvider);
	}

	//

	private CellStyle style;

	private final String pattern;
	private final BiFunction<Cell, D, K> valueProvider;

	/**
	 * Constructor for CellFiller.
	 *
	 * @param pattern The data format pattern.
	 * @param valueProvider A function that provides the value to be set in the cell.
	 */
	public CellFiller(final String pattern, final Function<D, K> valueProvider) {
		this(pattern, (c, d) -> valueProvider.apply(d));
	}

	/**
	 * Constructor for CellFiller.
	 *
	 * @param pattern The data format pattern.
	 * @param valueProvider A bi-function that provides the value to be set in the cell.
	 */
	public CellFiller(final String pattern, final BiFunction<Cell, D, K> valueProvider) {
		this.pattern = pattern;
		this.valueProvider = valueProvider;
	}

	/**
	 * Compiles the Excel cell using the provided workbook, cell, and data bean.
	 *
	 * @param wb The workbook object.
	 * @param cell The cell object.
	 * @param bean The data bean object.
	 */
	@SuppressWarnings("resource")
	public void compileCell(final Cell cell, final D bean) {
		if (this.style == null) {
			final Workbook wb = cell.getSheet().getWorkbook();
			this.style = wb.createCellStyle();
			this.style.setDataFormat(wb.createDataFormat().getFormat(this.pattern));
		}
		this.compileStyle(this.style);
		cell.setCellStyle(this.style);
		this.setCellValue(cell, this.valueProvider.apply(cell, bean));
	}

	/**
	 *
	 * @param cellStyle
	 */
	protected void compileStyle(final CellStyle cellStyle) {}

	protected void setCellValue(final Cell cell, final K value) {
		cell.setCellValue(Objects.toString(value, ""));
	}

	//
	/**
	 * A Boolean cell filler implementation.
	 */
	public static class BooleanCellFiller<D> extends CellFiller<D, Boolean> {

		public BooleanCellFiller(final Function<D, Boolean> valueProvider) {
			super("", valueProvider);
		}

		@Override
		protected void compileStyle(final CellStyle style) {
			style.setAlignment(HorizontalAlignment.CENTER);
		}

		@Override
		protected void setCellValue(final Cell cell, final Boolean value) {
			cell.setCellValue(value != null && value.booleanValue() ? "X" : "");
		}
	}

	/**
	 * A Date cell filler implementation.
	 */
	public static class DateCellFiller<D, K extends Date> extends CellFiller<D, K> {

		public DateCellFiller(final String pattern, final Function<D, K> valueProvider) {
			super(pattern, valueProvider);
		}

		@Override
		protected void compileStyle(final CellStyle style) {
			style.setAlignment(HorizontalAlignment.CENTER);
		}

		@Override
		protected void setCellValue(final Cell cell, final K value) {
			if (value == null) {
				cell.setCellValue("");
			} else {
				cell.setCellValue(value);
			}
		}
	}

	/**
	 * A LocalDate cell filler implementation.
	 */
	public static class LocalDateCellFiller<D> extends CellFiller<D, LocalDate> {

		public LocalDateCellFiller(final String pattern, final Function<D, LocalDate> valueProvider) {
			super(pattern, valueProvider);
		}

		@Override
		protected void compileStyle(final CellStyle style) {
			style.setAlignment(HorizontalAlignment.CENTER);
		}

		@Override
		protected void setCellValue(final Cell cell, final LocalDate value) {
			if (value == null) {
				cell.setCellValue("");
			} else {
				cell.setCellValue(value);
			}
		}
	}

	/**
	 * A LocalDateTime cell filler implementation.
	 */
	public static class LocalDateTimeCellFiller<D> extends CellFiller<D, LocalDateTime> {

		public LocalDateTimeCellFiller(final String pattern, final Function<D, LocalDateTime> valueProvider) {
			super(pattern, valueProvider);
		}

		@Override
		protected void compileStyle(final CellStyle style) {
			style.setAlignment(HorizontalAlignment.CENTER);
		}

		@Override
		protected void setCellValue(final Cell cell, final LocalDateTime value) {
			if (value == null) {
				cell.setCellValue("");
			} else {
				cell.setCellValue(value);
			}
		}
	}

	/**
	 * A Numeric cell filler implementation.
	 */
	public static class NumericCellFiller<D, K extends Number> extends CellFiller<D, K> {

		public NumericCellFiller(final String pattern, final Function<D, K> valueProvider) {
			super(pattern, valueProvider);
		}

		@Override
		protected void compileStyle(final CellStyle style) {
			style.setAlignment(HorizontalAlignment.RIGHT);
		}

		@Override
		protected void setCellValue(final Cell cell, final K value) {
			if (value == null) {
				cell.setCellValue("");
			} else {
				cell.setCellValue(value.doubleValue());
			}
		}
	}

	/**
	 * A Formula cell filler implementation.
	 */
	public static class FormulaCellFiller<D> extends CellFiller<D, String> {

		public FormulaCellFiller(final String pattern, final BiFunction<Cell, D, String> valueProvider) {
			super(pattern, valueProvider);
		}

		@Override
		protected void setCellValue(final Cell cell, final String value) {
			cell.setCellFormula(value);
		}

	}
}