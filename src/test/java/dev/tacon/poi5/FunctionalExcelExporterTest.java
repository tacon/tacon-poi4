package dev.tacon.poi5;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Period;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.junit.jupiter.api.Test;

import dev.tacon.poi5.ExcelExporter.ExcelFormat;

public class FunctionalExcelExporterTest {

	public static class GroupPeopleExcelExporter extends FunctionalExcelExporter<Person> {

		public GroupPeopleExcelExporter(final List<Person> dataList, final String name) {
			super(dataList, name);
		}

		@Override
		protected List<CellFiller<Person, ?>> getCellFillers() {
			final List<CellFiller<Person, ?>> cellFillers = List.of(
					CellFiller.forString(p -> p.lastName()),
					CellFiller.forString(p -> p.name()),
					CellFiller.forLocalDate(p -> p.birthDate(), "dd/MM/yyyy"),
					CellFiller.<Person> forInteger(x -> Integer.valueOf(Period.between(x.birthDate(), LocalDate.now()).getYears())),
					CellFiller.<Person> forDecimal(x -> x.amount()));
			return cellFillers;
		}

		@Override
		protected List<Group<Person, ?>> getGroups() {
			final Group<Person, BigDecimal> lastNameGroup = new Group<>(
					p -> p.lastName(),
					new Accumulator<Person, BigDecimal>() {

						@Override
						protected BigDecimal getInitialValue() {
							return BigDecimal.ZERO;
						}

						@Override
						protected BigDecimal accumulate1(final BigDecimal currentValue, final Person dto) {
							return currentValue.add(dto.amount());
						}

					},

					(w, d) -> {
						final Row row = w.createRow(w.getLastRowNum() + 1);

						final Cell cell = row.createCell(0);
						cell.setCellValue("People with last name: " + d.lastName());
						row.getSheet().addMergedRegion(new CellRangeAddress(row.getRowNum(), row.getRowNum(), 0, 4));

					},
					(w, d, v) -> {
						final Row row = w.createRow(w.getLastRowNum() + 1);
						final Cell cell = row.createCell(0);
						cell.setCellValue("Total ");
						row.getSheet().addMergedRegion(new CellRangeAddress(row.getRowNum(), row.getRowNum(), 0, 4));

						final Cell cell1 = row.createCell(5);
						CellFiller.forDecimal().compileCell(cell1, v);
					});
			final Group<Person, BigDecimal> granTotal = new Group<>(
					p -> "",
					new Accumulator<Person, BigDecimal>() {

						@Override
						protected BigDecimal getInitialValue() {
							return BigDecimal.ZERO;
						}

						@Override
						protected BigDecimal accumulate1(final BigDecimal currentValue, final Person dto) {
							return currentValue.add(dto.amount());
						}

					},
					(w, d, v) -> {
						final Row row = w.createRow(w.getLastRowNum() + 2);
						final Cell cell = row.createCell(0);
						cell.setCellValue("Total ");
						row.getSheet().addMergedRegion(new CellRangeAddress(row.getRowNum(), row.getRowNum(), 0, 3));

						// value with accumulator
						final Cell cell1 = row.createCell(4);
						CellFiller.forDecimal().compileCell(cell1, v);

						// value with formula
						final Cell cellFormula = row.createCell(5);
						CellFiller.forFormula("#0.00", (c, x) -> "SUM(F2:F" + c.getRowIndex() + ")").compileCell(cellFormula, null);

					});
			return List.of(granTotal, lastNameGroup);
		}

		@Override
		protected String[] getHeader() {
			return new String[] { "Last name", "Name", "Birth date", "Age", "Amount" };
		}
	}

	public static class SimplePeopleExcelExporter extends FunctionalExcelExporter<Person> {

		public SimplePeopleExcelExporter(final List<Person> dataList, final String name) {
			super(dataList, name);
		}

		@Override
		protected List<CellFiller<Person, ?>> getCellFillers() {
			final List<CellFiller<Person, ?>> cellFillers = List.of(
					CellFiller.forString(p -> p.lastName()),
					CellFiller.forString(p -> p.name()),
					CellFiller.forLocalDate(p -> p.birthDate(), "dd/MM/yyyy"),
					CellFiller.<Person> forInteger(x -> Integer.valueOf(Period.between(x.birthDate(), LocalDate.now()).getYears())),
					CellFiller.<Person> forDecimal(x -> x.amount()));
			return cellFillers;
		}

		@Override
		protected String[] getHeader() {
			return new String[] { "Last name", "Name", "Birth date", "Age", "Amount" };
		}
	}

	@Test
	void sheetWithGroup() throws IOException {
		final List<Person> people = List.of(
				new Person("Alice", "White", LocalDate.of(2000, 1, 1), new BigDecimal("123.456")),
				new Person("Bob", "White", LocalDate.of(2001, 2, 2), new BigDecimal("100")),
				new Person("Charlie", "Green", LocalDate.of(2002, 3, 1), new BigDecimal("200.01")));

		final GroupPeopleExcelExporter excelExporter = new GroupPeopleExcelExporter(people, "PeopleData");

		try (OutputStream os = new FileOutputStream(new File("fun.xlsx"))) {
			excelExporter.export(os, ExcelFormat.XLSX);
		}
	}

	@Test
	void simpleSheet() throws IOException {
		final List<Person> people = List.of(
				new Person("Alice", "White", LocalDate.of(2000, 1, 1), new BigDecimal("123.456")),
				new Person("Bob", "White", LocalDate.of(2001, 2, 2), new BigDecimal("100")),
				new Person("Charlie", "Green", LocalDate.of(2002, 3, 1), new BigDecimal("200.01")));

		final SimplePeopleExcelExporter excelExporter = new SimplePeopleExcelExporter(people, "PeopleData");

		try (OutputStream os = new FileOutputStream(new File("simple.xlsx"))) {
			excelExporter.export(os, ExcelFormat.XLSX);
		}
	}

}
