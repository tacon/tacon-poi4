package dev.tacon.poi5;

import java.math.BigDecimal;
import java.time.LocalDate;

record Person(String name, String lastName, LocalDate birthDate, BigDecimal amount) {}